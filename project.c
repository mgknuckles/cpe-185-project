#include <stdio.h>
#include <wiringPi.h>

const int ledPin1 = 3; //Pi pin 5
const int irPin1 = 2; //Pi pin 3 
const int ledPin2 = 27;	//Pi pin 13
const int irPin2 = 17;  //Pi pin 11
const int ledPin3 = 9; //Pi pin 21
const int irPin3 = 10; //Pi pin 19
const int ledPin4 = 6; //Pi pin 29
const int irPin4 = 5; //Pi pin 31



int main(void)
{
	wiringPiSetupGpio();
	pinMode(irPin1, INPUT);
	pinMode(irPin1, OUTPUT);
	
	pinMode(irPin2, INPUT);	
	pinMode(irPin2, OUTPUT);

	pinMode(irPin3, INPUT);
	pinMode(irPin3, OUTPUT);
	
	pinMode(irPin4, INPUT);
	pinMode(irPin4, OUTPUT);

	while(1)
	{
		if(digitalRead(irPin1)){
			digitalWrite(ledPin1, HIGH);
			printf("Sensor 1 Triggered!");
		}else
			digitalWrite(ledPin1, LOW):
	}
	while(1)
	{	
		if(digitalRead(irPin2)){
			digitalWrite(ledPin2, HIGH);
			printf("Sensor 2 Triggered!!");
		}else
			digitalWrite(ledPin2, LOW);
	}
	while(1)
	{
		if(digitalRead(irPin3)){
			digitalWrite(ledPin3, HIGH);
			printf("Sensor 3 TRIGGERED!");
		}else
			digitalWrite(ledPin3, LOW);
	}
	return 0;
}
